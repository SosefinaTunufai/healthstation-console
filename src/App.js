import React, {Component} from 'react';
import axios from 'axios';

import './App.css';
import Row from './components/Row';

class App extends Component {

    constructor() {
        super();
        this.state = {websites: []};
    };

    componentDidMount() {

        axios.get('http://localhost:8002/logs/latest')
            .then(result => {
                this.setState({
                    websites: result.data
                });
            });
    };

    renderRows() {
        return this.state.websites.map(website => {
            return (
                <Row url={website.url}
                     status={website.status}
                     key={website.url}/>
            )
        });
    }

    render() {
        return (
            <table className="table">
                <tbody>
                {this.renderRows()}
                </tbody>
            </table>
        );
    }
}

export default App;