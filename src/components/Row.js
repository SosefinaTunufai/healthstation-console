import React, {Component} from 'react';

const Row = (props) => {

    const statusClass = props.status === 200 ? 'status  status--ok' : 'status  status--error';

    return (
        <tr className="row">
            <td>{props.url}</td>
            <td className={statusClass}>{props.status}</td>
        </tr>
    )
};

export default Row;